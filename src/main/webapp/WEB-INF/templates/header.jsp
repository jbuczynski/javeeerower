<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id= "authenticationService" scope= "session" class= "service.AuthenticationService" />
<jsp:useBean id= "registrationService" scope= "session" class= "service.RegistrationService" />
<jsp:setProperty name="authenticationService" property="session" value="${pageContext.session}"/>
<div id="header" class="page-header ">
        <script type="text/javascript">
        function visible_hidden(boolean){
            var y = document.getElementById("loggingForm");
            y.hidden = boolean;
        }
            </script>
        <c:choose>
            <c:when test="${authenticationService.loggedIn}">
               <h1 class="row"><p class="text-center"> Witaj na stronie o rowerach ${authenticationService.currentUser.name}</p></h1>
               <div class="row pull-right">
                   <div class="col-md-6"><p class="pull-right "><a href="logOutServlet">Wyloguj</a></p></div>
                   <div class="col-md-6"><p class="pull-right "><a href="register.jsp">Rejestracja</a></p></div>
                </div>
            </c:when>
            <c:otherwise>
              <h1 class="row"><p class="text-center"> Witaj na stronie o rowerach</p></h1>
              <div class="row pull-right">
                   <div class="col-md-6"><p class="pull-right "><a href="#" onclick="visible_hidden(false)">Zaloguj</a></p></div>
                   <div class="col-md-6"><p class="pull-right "><a href="register.jsp">Rejestracja</a></p></div>
               </div>
            </c:otherwise>
        </c:choose>
</div>