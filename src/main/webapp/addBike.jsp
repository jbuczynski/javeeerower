<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<c:if test="${!sessionScope.authenticationService.loggedIn}">
  <c:redirect url="index.jsp"/>
</c:if>
<t:pageTemplate>
    <jsp:attribute name="header">
      <c:import url="WEB-INF/templates/header.jsp" />
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
       <h3><p class="text-center">Proszę wypełnić dane o rowerze</p></h3>
       <form action="dodajRower"  method="POST" class="container form">
         <div class="row"><p class="col-md-1">Moodel :</p><input type="text" value="${model}" class="col-md-2" name="model" required /></div>
         <div class="row"><p class="col-md-1"> Kolor :</p><input type="text" value="${kolor}" class="col-md-2"  name="kolor"  required/></div>
         <div class="row"><p class="col-md-1"> Numer seryjny :</p><input type="text" value="${numerSeryjny}"class="col-md-2"  name="numerSeryjny" required /></div>
         <div class="row"><p class="col-md-1"> Zdjęcie :</p><input type="file" class="col-md-2"  name="zdjecie"  /></div>
         <div class="row"><p class="col-md-1"> Date :</p><input type="date" value="${data}" class="col-md-2"  name="dataProdukcji" required /></div>


         <input type="submit" class="button" value=" OK " >

       </form>
       <p class="text-danger">${komunikatBledu}</p>
    </jsp:body>
</t:pageTemplate>



