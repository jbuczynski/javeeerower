<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="roweryObj" class="service.StorageService" scope="session" />
<t:pageTemplate>

    <jsp:attribute name="header">
      <c:import url="WEB-INF/templates/header.jsp" />
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>


         <c:forEach var="rower" items="${roweryObj.rowery}">
         	<div class="row thumbnail">
         		<div class="col-md-4">

         				<table class="table">
        	 				<tr>
        					    <th>Model:</th>
        					    <th>nr seryjny:</th>
        					    <th>kolor</th>
        					     <th>data produkcji:</th>
        				  	</tr>
        		 			<tr>
        			 			<td>
        			 				<c:out value="${rower.model}" />
        						</td>
        						<td>
        			 				<c:out value="${rower.numerSeryjny}" />
        						</td>
        						<td>
        			 				<c:out value="${rower.kolor}" />
        						</td>
        						<td>
        			 				<c:out value="${rower.dataProdukcji}" />
        						</td>

        					</tr>
        				</table>

         		</div>
         		<div class="col-md-8">
                	<img alt="logo" src=<c:out value="resources/img/rower1.jpg"/> width="30%" height="20%" class="center-block" />
                </div>
         	</div>
        </c:forEach>


        <p>
          <a href="addBike.jsp">Dodaj nowy rower</a>
        </p>

    </jsp:body>
</t:pageTemplate>

