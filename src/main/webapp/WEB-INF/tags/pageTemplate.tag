<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<html>
  <head>
    <link href="<c:url value='/resources/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  </head>
  <body>
    <div id="pageheader">
      <jsp:invoke fragment="header"/>
    </div>
    <div id="body" class="container">
      <jsp:doBody/>
    </div>
    <div id="pagefooter">
        <div  class="navbar navbar-default navbar-fixed-bottom">
              <p class="footer" id="copyright">Copyright 1927, fabryka rowerów FABROW.</p>
              <div class="pull-right">
               <h:messages infoStyle="color:green" layout="table"  />
              </div>
        </div>
      <jsp:invoke fragment="footer"/>
    </div>
  </body>
</html>