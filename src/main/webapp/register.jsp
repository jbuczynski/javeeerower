<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<t:pageTemplate>
    <jsp:attribute name="header">
      Rejestracja użytkownika
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
       <h3><p class="text-center">Proszę wypełnić dane użytkownika</p></h3>
       <form action="registerUser"  method="POST" class="container form">
         <div class="row"><p class="col-md-1">uzytkownik :</p><input type="text" value="admin" class="col-md-2" name="username" required /></div>
         <div class="row"><p class="col-md-1"> hasło :</p><input type="password"  class="col-md-2"  name="password"  required/></div>

         <input type="submit" class="button" value=" OK " >

       </form>
       <p class="text-danger">${komunikatBledu}</p>
    </jsp:body>
</t:pageTemplate>