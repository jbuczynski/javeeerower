package controllers;

import myModel.User;
import service.AuthenticationService;
import service.RegistrationService;

import javax.enterprise.context.RequestScoped;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by Jakub on 2014-11-23.
 */
@WebServlet(urlPatterns = "/logOutServlet")
public class LogOut extends HttpServlet{

    private HttpSession session;

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        try {
            perform(arg0, arg1);
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        try {
            perform(arg0, arg1);
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            e.getMessage();
        }

    }

    protected void perform(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException,
            NumberFormatException, ParseException {
        response.setContentType("text/html");
        session = request.getSession();

        ((AuthenticationService)(session.getAttribute("authenticationService"))).setLoggedIn(false);
        response.sendRedirect("index.jsp");
    }

}
