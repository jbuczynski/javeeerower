package controllers;

import myModel.User;
import service.AuthenticationService;
import service.RegistrationService;

import javax.enterprise.context.RequestScoped;
import javax.naming.AuthenticationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by Jakub on 2014-11-23.
 */
@WebServlet(urlPatterns = "/logInServlet")
public class LogIn extends HttpServlet{

    private HttpSession session;

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        try {
            perform(arg0, arg1);
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest arg0, HttpServletResponse arg1)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        try {
            perform(arg0, arg1);
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            e.getMessage();
        }

    }

    protected void perform(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException,
            NumberFormatException, ParseException {
        response.setContentType("text/html");
        session = request.getSession();

        Map<String, String[]> parameters = request.getParameterMap();
        String name = parameters.get("username")[0];
        String password = parameters.get("password")[0];


            if ( ! ((RegistrationService) (session.getAttribute("registrationService"))).userExists(name) ) {
               // response.sendRedirect("WEB-INF/templates/404NotFound.html");
                request.setAttribute("komunikatBledu", "niepoprawne dane");
                RequestDispatcher view = getServletContext()
                        .getRequestDispatcher("/index.jsp");

                view.forward(request, response);
            }
        else {
                User user = new User(name,password);
                AuthenticationService service =((AuthenticationService) (session.getAttribute("authenticationService")));
               if( service.validateUser(user) ) {
                   service.setLoggedIn(true);
                   service.setCurrentUser(user);
                   response.sendRedirect("index.jsp");
               } else {

                   request.setAttribute("komunikatBledu", "niepoprawne dane");
                   RequestDispatcher view = getServletContext()
                           .getRequestDispatcher("/index.jsp");

                   view.forward(request, response);
               }
            }
    }

}
