package service;

import myModel.User;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.security.sasl.AuthenticationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jakub on 2014-11-23.
 */
@SessionScoped
public class RegistrationService {

   // private Set registeredUsers = new HashSet<User>() ;
    private Map<String, User> registeredUsers = new HashMap<String, User>();
    public RegistrationService() {

    }

    public void registerUser(User user) {
        registeredUsers.put(user.getName(), user);
    }
    public  Boolean userExists(String username)  {

        if( !registeredUsers.containsKey(username) ) {
            return false;
        }
        else {
            return true;
        }

    }

    public Boolean validateUserData(User user) {
        User existing = registeredUsers.get(user.getName());
        if( existing.getPassword().equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    public  Boolean userExists(User user)  {
       if( !registeredUsers.containsValue(user.getName())) {
           return false;
        }
        else {
           return true;
       }

    }

}
