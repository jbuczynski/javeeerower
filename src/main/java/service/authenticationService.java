package service;

import myModel.User;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpSession;

/**
 * Created by Jakub on 2014-11-23.
 */

@SessionScoped
public class AuthenticationService {

    private HttpSession session;
    private Boolean loggedIn = false;
    private User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public void setLoggedIn(Boolean state) {
        loggedIn = state;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }


    public Boolean validateUser(User user) throws AuthenticationException {
        //currentUser = (User) session.getAttribute("currentUser");
        if ( ((RegistrationService)(session.getAttribute("registrationService"))).userExists(user.getName()) ) {
            if(((RegistrationService)(session.getAttribute("registrationService"))).validateUserData(user)) {
                return true;
            }
            else {
                return false;
            }
        } else {
            throw new AuthenticationException("User is logged but not registred!!!!!");
        }
    }



}



