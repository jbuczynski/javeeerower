package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myModel.Rower;
import service.StorageService;

@WebServlet(urlPatterns = "/dodajRower")
public class AddBikeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			perform(arg0, arg1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			perform(arg0, arg1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.getMessage();
		}

	}

	protected void perform(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			NumberFormatException, ParseException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		try {

			Map<String, String[]> parametry = request.getParameterMap();

			if (AddValidator.validateParameters(parametry)) {
				Rower r = new Rower(
						Rower.Kolor.valueOf(parametry.get("kolor")[0]),
						parametry.get("model")[0], Integer.valueOf(parametry
								.get("numerSeryjny")[0]), "zdjecie",
						new SimpleDateFormat("yyyy-mm-dd").parse(parametry.get("dataProdukcji")[0]));
				StorageService storageService;
				if(request.getSession().getAttribute("roweryObj") != null)
					storageService = (StorageService) request
							.getSession().getAttribute("roweryObj");
				else storageService = new StorageService();
				
				storageService.addRower(r);

				out.println("<html><body><h3>Dodano rower: </h3>"
						+ "<p>Kolor: " + parametry.get("kolor")[0] + "<br />"
						+ "<a href=\"showBikes.jsp\">Pokaz wszystkie</a>"
						+ "</body></html>");
				// out.close();
			} else {
				request.setAttribute("komunikatBledu", "Wprowadzono nieporawne dane");
				request.setAttribute("kolor",parametry.get("kolor")[0] );
				request.setAttribute("model",parametry.get("model")[0] );
				request.setAttribute("numerSeryjny",parametry.get("numerSeryjny")[0] );
				request.setAttribute("data",parametry.get("dataProdukcji")[0] );
               RequestDispatcher view = getServletContext()
						.getRequestDispatcher("/addBike.jsp");

				view.forward(request, response);

				//response.sendRedirect("addBike.jsp");

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.close();
		}

	}

}
