package controllers;

import java.util.HashMap;
import java.util.Map;

import myModel.Rower;

public class AddValidator {
	public static boolean validateParameters(Map<String, String[]> parametry)
	{
		System.out.println(validateKolor(parametry));
		return validateKolor(parametry);
		
	}
	
	private static boolean validateKolor(Map<String, String[]> params)
	{

		String[] kolory  = params.get("kolor");
		
		for(String s : kolory)
		{
			if(Rower.Kolor.getByValue(s) == null )
				return false;
		}
		
		return true;
	}
	
}
