package myModel;

import java.util.Date;

public class Rower {
	public static enum Kolor {
		
		zielony, niebieski, czerwony, czarny;
		
		public String toString()
		{
		  switch(this){
	        case zielony :
	            return "zielony";
	        case niebieski :
	            return "niebieski";
	        case czerwony :
	            return "czerwony";
	        case czarny:
	        	return "czarny";
	        }
	        return null;
		}
		public static Kolor getByValue(String value){
		        if(value.equalsIgnoreCase(zielony.toString()))
		            return Kolor.zielony;
		        else if(value.equalsIgnoreCase(niebieski.toString()))
		            return Kolor.niebieski;
		        else if(value.equalsIgnoreCase(czerwony.toString()))
		            return Kolor.czerwony;
		        else if(value.equalsIgnoreCase(czarny.toString()))
		            return Kolor.czarny;
		        else
		            return null;
		    }
	
	};
	
	public enum TypRamy
	{
		mala,srednia,duza;
		
		public String toString()
			{
			  switch(this){
		        case mala :
		            return "mala";
		        case srednia :
		            return "srednia";
		        case duza :
		            return "duza";
		        }
		        return null;
			}
		

	};
	
	
	private Kolor kolor;
	private String model;
	private int numerSeryjny;
	private String zdjeciePath;
	private Date dataProdukcji;
	
	public Rower(){};
	public Rower(myModel.Rower.Kolor kolor, String model, int numerSeryjny,
			String zdjeciePath, Date dataProdukcji) {
		super();
		this.kolor = kolor;
		this.model = model;
		this.numerSeryjny = numerSeryjny;
		this.zdjeciePath = zdjeciePath;
		this.dataProdukcji = dataProdukcji;
	}

	public Kolor getKolor() {
		return kolor;
	}

	public void setKolor(Kolor kolor) {
		this.kolor = kolor;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getNumerSeryjny() {
		return numerSeryjny;
	}

	public void setNumerSeryjny(int numerSeryjny) {
		this.numerSeryjny = numerSeryjny;
	}

	public String getZdjeciePath() {
		return zdjeciePath;
	}

	public void setZdjeciePath(String zdjeciePath) {
		this.zdjeciePath = zdjeciePath;
	}

	public Date getDataProdukcji() {
		return dataProdukcji;
	}

	public void setDataProdukcji(Date dataProdukcji) {
		this.dataProdukcji = dataProdukcji;
	}
	
	
	
}

