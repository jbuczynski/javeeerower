package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myModel.Rower;

public class StorageService {

	private List<Rower> rowery = new ArrayList<Rower>();
	
	public StorageService()
	{	
		Rower r = new Rower( Rower.Kolor.czarny , "cross 1500",125125365,
				"resources/img/rower1.jpg", new Date() );
			
			rowery.add(r);
	}
	
	public void addRower(Rower rower)
	{
		Rower r = new Rower(rower.getKolor(), rower.getModel(), rower.getNumerSeryjny(),
			rower.getZdjeciePath(), rower.getDataProdukcji());
		
		rowery.add(r);
	}
	public List<Rower> getRowery()
	{
		return rowery;
	}
	
		
	
}
