<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<t:pageTemplate>

    <jsp:attribute name="header">
      <c:import url="WEB-INF/templates/header.jsp" />
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
         <h2>Moja strona o rowerze</h2>
                <div class="row">
                    <c:if test="${sessionScope.authenticationService.loggedIn}">
                        <p><a href="addBike.jsp">Dodaj nowy rower</a></p>
                    </c:if>

                   <p><a href="showBikes.jsp">Zobacz wszystkie rowery</a></p>
               </div>
                <div class="container">
                <form action="logInServlet" hidden="true" id="loggingForm" method="post" class=" form pull-right">
                          Użytkownik: <input type="text" name="username"  />
                          Hasło: <input type="password" name="password"  />
                           <input type="submit" value="Loguj" />
                       </form>
                        <p class="text-danger">${komunikatBledu}</p>
               </div>
    </jsp:body>
</t:pageTemplate>
